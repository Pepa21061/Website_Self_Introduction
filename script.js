// Get the image element
const image = document.getElementById('zoom-image');
const image2 = document.getElementById('zoom-image2');
const image3 = document.getElementById('zoom-image3');
const image4 = document.getElementById('zoom-image4');

// Add click event listener to the image
image.addEventListener('click', function() {
    
    if (image.style.width === '776px') {
        // If so, revert back to original size
        image.style.width = '388px'; // Original width
        image.style.height = 'auto'; // Maintain aspect ratio
    } else {
        // Otherwise, change width and height to higher resolution
        image.style.width = '776px'; // Higher resolution width
        image.style.height = 'auto'; // Maintain aspect ratio
    }
});

image2.addEventListener('click', function() {
    
    if (image2.style.width === '570px') {
        // If so, revert back to original size
        image2.style.width = '285px'; // Original width
        image2.style.height = 'auto'; // Maintain aspect ratio
    } else {
        // Otherwise, change width and height to higher resolution
        image2.style.width = '570px'; // Higher resolution width
        image2.style.height = 'auto'; // Maintain aspect ratio
    }
});

image3.addEventListener('click', function() {
    
    if (image3.style.width === '550px') {
        // If so, revert back to original size
        image3.style.width = '275px'; // Original width
        image3.style.height = 'auto'; // Maintain aspect ratio
    } else {
        // Otherwise, change width and height to higher resolution
        image3.style.width = '550px'; // Higher resolution width
        image3.style.height = 'auto'; // Maintain aspect ratio
    }
});

image4.addEventListener('click', function() {
    
    if (image4.style.width === '600px') {
        // If so, revert back to original size
        image4.style.width = '300px'; // Original width
        image4.style.height = 'auto'; // Maintain aspect ratio
    } else {
        // Otherwise, change width and height to higher resolution
        image4.style.width = '600px'; // Higher resolution width
        image4.style.height = 'auto'; // Maintain aspect ratio
    }
});